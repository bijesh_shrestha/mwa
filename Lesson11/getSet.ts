class Person{
    private _firstName = '';

    get firstName(){
        return this._firstName;
    }

    set firstName(name: string){
        this._firstName = name.toUpperCase();
    }
}

let person: Person = new Person();
person.firstName = 'Bijesh';
console.log(person.firstName);