class BaseObject{
    width: number = 0;
    length: number = 0;

    calcSize(){
        return this.width * this.length;
    }
}

let rectangle: BaseObject = new BaseObject();
rectangle.width = 5;
rectangle.length = 2;

console.log(rectangle.calcSize());