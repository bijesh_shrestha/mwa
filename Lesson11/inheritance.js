"use strict";
var BaseObject = /** @class */ (function () {
    function BaseObject() {
        this.width = 0;
        this.length = 0;
    }
    BaseObject.prototype.calcSize = function () {
        return this.width * this.length;
    };
    return BaseObject;
}());
var rectangle = new BaseObject();
rectangle.width = 5;
rectangle.length = 2;
console.log(rectangle.calcSize());
