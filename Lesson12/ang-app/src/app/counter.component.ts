import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter',
  template: `
    <button (click)="decrease()">-</button>
    {{counterValue}}
    <button (click)="increase()">+</button>
  `,
  styles: []
})
export class CounterComponent implements OnInit {

  counterValue: number = 0;
  @Input() counter;
  @Output() counterChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.counterValue = this.counter;
  }

  increase(){
    this.counterChange.emit(++this.counterValue);
  }

  decrease(){
    this.counterChange.emit(--this.counterValue);
  }

}
