const tree = { 
	name : "home", 
	files : ["notes.txt","todo.txt"], 
	subFolders: [	
		{ name : "payroll", 
		  files : ["paper.pdf","funds.csv"], 
		  subFolders: [] 
		}, 
		{ name: "misc", 
		  files : ["summer1.jpg","summer2.jpg", "summer3.jpg"], 
		  subFolders: [
			{ name : "logs", 
			  files : ["logs1","logs2","logs3","logs4"], 
			  subFolders: [] 
		  }] 
	}] 
}; 
// function find(fileName) {
//     return function(obj){
//         for(let j=0; j<obj.files.length; j++){
//             if(fileName==obj.files[j]){
//                 return true;
//             }else{                
//                 for(let i=0; i<obj.subFolders.length; i++){
//                     let result = find(fileName)(obj.subFolders[i]);   
//                     if (result !== false) {
//                         return result;
//                     }       
//                 }
//                 return false;
//             }
//         }
//     }
// }

// const find = (fileName) => {
//     return (obj) => {
//         for(let j=0; j<obj.files.length; j++){
//             if(fileName==obj.files[j]){
//                 return true;
//             }else{                
//                 for(let i=0; i<obj.subFolders.length; i++){
//                     let result = find(fileName)(obj.subFolders[i]);   
//                     if (result !== false) {
//                         return result;
//                     }       
//                 }
//                 return false;
//             }
//         }
//     }
// }

const find = fileName => obj => {
    if(obj.files.includes(fileName))
        return true;
    for(const subFolder of obj.subFolders){
        if (find(fileName)(subFolder))
            return true;
    }
    return false;
};

console.log(find("paper.pdf")(tree)); // true 
console.log(find("randomfile")(tree)); // false