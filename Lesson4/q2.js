const os = require('os');
const Rx = require('@reactivex/rxjs');

const checkSystem = ()=>{
    return new Promise((resolve, reject)=>{
        if((os.totalmem()/(1024*1024*1024))<4){
            reject('This app needs at least 4GB of RAM');
        } else if(os.cpus().length<=2){
            reject('Processor is not supported');
        } else {
            resolve('System is checked successfully');
        }
    })
};

Rx.Observable.fromPromise(checkSystem())
                .subscribe(e=>console.log(e));

// checkSystem().then(data=>console.log(data))
//                 .catch(err=>console.log(err));
console.log('Checking your system...');