const http = require('http');
const {fork} = require('child_process');
const server = http.createServer();

server.on('request', (req, res) => {
    const childProcess = fork('fileLoader.js');
    childProcess.send(res);
    childProcess.on('message', file=>{
        res.end(file);
    });
});

server.listen(3000);