const fs = require('fs');

const response;

const fileLoad = ()=>{
    const src = fs.createReadStream('./file.txt');
    return src.pipe(response);
};

process.on('message', (res)=>{
    response = res;
    const file = fileLoad();
    process.send(file);
});