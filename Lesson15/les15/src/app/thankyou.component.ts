import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-thankyou',
  template: `
    <p>
      Thankyou!
    </p>
  `,
  styles: []
})
export class ThankyouComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
