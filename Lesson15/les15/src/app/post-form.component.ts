import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { JsonServiceService } from './json-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styles: []
})
export class PostFormComponent {

  myForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private jsonService: JsonServiceService, private router: Router) {
    this.myForm = formBuilder.group({
      'name': ['ddd', Validators.required],
      'email': ['mmm', [Validators.required, Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]],
      'post': ['Testing', [Validators.required, this.checkPostLength]]
    });
  }

  checkPostLength(control: FormControl): { [s: string]: boolean } {
    if (control.value.length < 10) {
      return { 'post': true };
    }
    return null;
  }

  getData(){
    this.jsonService.getUserData().subscribe(
      data=>{
        this.myForm.controls['name'].setValue(data['name']);
        this.myForm.controls['email'].setValue(data['email']);
      },
      err=>console.error(err),
      ()=>console.log("data loaded")
    );

    this.jsonService.getPosts().subscribe(
      data=>{
        this.myForm.controls['post'].setValue(data[0]['body']);
      },
      err=>console.error(err),
      ()=>console.log("data loaded")
    );
  }

  onSubmit(){
    console.log(this.myForm.value);
    this.router.navigate(['/thankyou']);
  }

}
