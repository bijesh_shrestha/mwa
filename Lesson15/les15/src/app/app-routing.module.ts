import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThankyouComponent } from './thankyou.component';
import { PostFormComponent } from './post-form.component';

const routes: Routes = [
  { path: '', component : PostFormComponent},
  { path: 'thankyou', component : ThankyouComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
