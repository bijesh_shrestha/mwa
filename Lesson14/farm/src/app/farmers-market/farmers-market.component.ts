import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';

@Component({
  selector: 'app-farmers-market',
  template: `
    <ul>
      <li *ngFor="let farm of farms"><a href={{farm.link}}>{{farm.Farm}}</a></li>
    </ul>
  `,
  styles: []
})
export class FarmersMarketComponent implements OnInit {

  farms: any;

  constructor(private dbService: DbService) { }

  ngOnInit() {
    this.farms = this.dbService.getData();
  }

}
