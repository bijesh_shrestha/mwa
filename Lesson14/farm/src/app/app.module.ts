import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FarmersMarketComponent } from './farmers-market/farmers-market.component';
import { FarmDetailsComponent } from './farm-details/farm-details.component';
import { FarmGuard } from './farm-details/farm.guard';
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    FarmersMarketComponent,
    FarmDetailsComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [FarmGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
