import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs";
import { DbService } from "../db.service";
import { Injectable } from "@angular/core";

@Injectable()
export class FarmGuard implements CanActivate {

    constructor(private dbService: DbService, private router: Router){ }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        if(this.dbService.hasId(route.params.id)) return true;
        else {this.router.navigate(['/error']); return false;}
    }
    
}