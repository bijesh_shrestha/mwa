import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { DbService } from '../db.service';

@Component({
  selector: 'app-farm-details',
  template: `
    <div>
      {{details.Farm}}
      <ul>
        <li *ngFor="let i of details.produce">{{i}}</li>  
      </ul>
    </div>
  `,
  styles: []
})
export class FarmDetailsComponent {

  private subscription: Subscription;
  id: string;
  private details: any;

  constructor(private activatedRoute: ActivatedRoute, private dbService: DbService ) {
    this.subscription = activatedRoute.params.subscribe(
        (param: any) => {
          this.id = param['id'];
          this.details = dbService.getDataById(this.id);
        }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
