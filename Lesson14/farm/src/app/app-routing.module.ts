import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FarmersMarketComponent } from './farmers-market/farmers-market.component';
import { FarmDetailsComponent } from './farm-details/farm-details.component';
import { FarmGuard } from './farm-details/farm.guard';
import { ErrorComponent } from './error/error.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: 'market', component: FarmersMarketComponent },
  { path: 'farm/:id', component: FarmDetailsComponent, canActivate:[FarmGuard] },
  { path: 'error', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
