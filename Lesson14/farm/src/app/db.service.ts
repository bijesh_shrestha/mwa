import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  data: any = [
    {_id:1, Farm:'Natural Prairie', produce:['lettuce', 'tomato']},
    {_id:2, Farm:'Fairfield Farm', produce:['potato', 'cranberry', 'tangerine']}
  ];

  constructor() { }

  getData(): any {
    this.data.forEach(element => {
      element.link = "/farm/"+element._id;
    });
    return this.data;
  }

  getDataById(id): any {
    for (const iterator of this.data) {
      if(id==iterator._id)
        return iterator;
    }
  }

  hasId(id): boolean {
    for (const iterator of this.data) {
      if(id==iterator._id)
        return true;
    }
    return false;
  }

}
