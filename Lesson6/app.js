const express = require('express');
const morgan = require('morgan');
const expressValidator = require('express-validator');
const { check, validationResult } = require('express-validator/check');
const cors = require('cors');

const app = express();

app.use(express.urlencoded({extended:false}));
app.use(morgan('combined'));
app.use(expressValidator());
app.use(cors());

let grades = [
    {
        id: 1,
        name: "bijesh",
        course: "cs572",
        grade: 95
    },
    {
        id:2,
        name: "test",
        course: "t123",
        grade: 95
    }
];

app.get('/grades', (req, res)=>{
    res.json(grades);
});

app.get('/grades/:id', (req, res)=>{
    const id = req.params.id;
    const grade = grades.filter(g=>g.id==id);
    res.json(grade);
});

app.post('/grades', (req, res)=>{
    req.assert('name', 'Name is required').notEmpty();
    req.assert('course', 'Course length must be minimum 5 letters').isLength(5);

    const errors = req.validationErrors();
    if (errors) {
        return res.status(422).json({ errors: errors });
    }
    console.log(req.body.name);
    console.log(req.body.course);
    console.log(req.body.grade);
    res.json(req.body);
});

app.listen(3000, ()=>console.log('Server started at port 3000...'));