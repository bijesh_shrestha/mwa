const express = require('express');
const mongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const expressValidator = require('express-validator');
const { check, validationResult } = require('express-validator/check');

const app = express();

app.use(express.urlencoded({extended:false}));
app.use(expressValidator());

app.get('/', (req, res)=>{
    mongoClient.connect('mongodb://127.0.0.1:27017/mwa', (err, client)=>{
        if(err) throw err;
        const db = client.db('mwa');
    
        db.collection('locations').find({}).toArray((err, result)=>{
            if(err) throw err;
            res.json(result);
            client.close();
        });
    });
});

app.post('/newLocation', (req, res)=>{
    mongoClient.connect('mongodb://127.0.0.1:27017/mwa', (err, client)=>{
        if(err) throw err;
        const db = client.db('mwa');
        
        db.collection('locations').insert({name:req.body.name, category:req.body.category, location:[parseFloat(req.body.longitude), parseFloat(req.body.latitude)]}, (err, records)=>{
            if(err) throw err;
            res.send('Record added');
            client.close();
        });
    });
});

app.put('/updateLocation', (req, res)=>{
    mongoClient.connect('mongodb://127.0.0.1:27017/mwa', (err, client)=>{
        if(err) throw err;
        const db = client.db('mwa');
        
        db.collection('locations').update({_id:ObjectID(req.body.id)}, {$set:{name:req.body.name, category:req.body.category, location:[parseFloat(req.body.longitude), parseFloat(req.body.latitude)]}}, (err, records)=>{
            if(err) throw err;
            res.send('Record updated');
            client.close();
        });
    });
});

app.delete('/deleteLocation', (req, res)=>{
    mongoClient.connect('mongodb://127.0.0.1:27017/mwa', (err, client)=>{
        if(err) throw err;
        const db = client.db('mwa');
        
        db.collection('locations').remove({_id:ObjectID(req.body.id)}, (err, records)=>{
            if(err) throw err;
            res.send('Record deleted');
            client.close();
        });
    });
});

// To create geospatial index
// db.locations.createIndex({location:'2d'});

app.get('/nearBy', (req, res)=>{
    req.assert('category', 'Category is required').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        return res.status(422).json({ errors: errors });
    }
    
    let name;
    if(req.query.name)
        name = req.query.name;
    else
        name = '';

    mongoClient.connect('mongodb://127.0.0.1:27017/mwa', (err, client)=>{
        if(err) throw err;
        const db = client.db('mwa');
    
        db.collection('locations').find({location:{$near:[-91.9688257, 41.022157]}, category: {$regex: req.query.category}, name: {$regex: name}}).limit(3).toArray((err, result)=>{
            if(err) throw err;
            res.json(result);
            client.close();
        });
    });
});

app.listen(3000, ()=>console.log('Server started at port 3000...'));