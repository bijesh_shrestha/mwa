import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child',
  template: `
    <ul>
      <li *ngFor="let str of strArray" appUpper appMyvisibility [visible]=true appMycolor (sendColor)="getColor($event)">{{str}}</li>
    </ul>
  `,
  styles: []
})
export class ChildComponent implements OnInit {

  @Input() strArray: string[];

  constructor() { }

  ngOnInit() {
  }

  getColor(data){
    console.log(data);
  }

}
