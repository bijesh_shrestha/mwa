import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[appMyvisibility]'
})
export class MyvisibilityDirective {

  @Input() visible: boolean = false;

  constructor(private e: ElementRef) { }

  ngOnInit(){
    if(this.visible) this.e.nativeElement.style.visibility = "visible";
    else this.e.nativeElement.style.visibility = "hidden";
  }

}
