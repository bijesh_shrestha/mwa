import { Directive, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appMycolor]'
})
export class MycolorDirective {

  colors: string[] = ['red', 'blue', 'green', 'orange'];
  index:number = 0;
  @Output() sendColor = new EventEmitter();

  constructor(private e: ElementRef) { }

  @HostListener('click') changeColor(){ 
    this.e.nativeElement.style.color = this.colors[this.index];
    this.sendColor.emit(this.colors[this.index]);
    this.index++;
    if(this.index==this.colors.length)
      this.index = 0;
  }

}
