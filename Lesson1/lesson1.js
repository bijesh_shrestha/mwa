// 1.
var array = new Array(1,3,-1,5,2,2);
function isDual() {
    if(this.length==0)
        return 1;
    if(this.length%2!=0)
        return 0;
    let reqSum = this[0]+this[1];
    for(let i=0; i<this.length; i++){
        if(i%2==0){
            if(this[i]+this[i+1]!=reqSum){
                return 0;
            }
        }
    }
    return 1;
}
Array.prototype.isDual = isDual;
console.log(array.isDual());

// 2.
function isWeekend(){
    const todayDate = new Date();
    const day = todayDate.getDay();
    return (day==0 || day==6)?'weekend':'weekday';
}
console.log(isWeekend());

// 3.
var applyCoupon = function(category){
    return function(discount){
        return function(item){
            console.log(item.price * (1 - discount));
        }
    }
}
const item = {
    "name":"Biscuits",
    "type":"regular",
    "category":"food",
    "price":2.0
}
applyCoupon("food")(0.1)(item);