const express = require('express');
const fetch = require('node-fetch');
const rxjs = require('rxjs');
const {Observable, from} = rxjs;
const { map } = rxjs.operators;

const app = express();

app.set('x-powered-by', false);
app.set('strict routing', true);
app.enable('case sensitive routing');

// promise
// app.get('/users', (req, res) => {
//     fetch('http://jsonplaceholder.typicode.com/users/')
//         .then(data => data.json())
//         .then(data => res.json(data))
//         .catch(err => console.log(err));
// });

// async await
// app.get('/users', (req, res) => {
//     getUsers(res);
// });
// async function getUsers(response) {
//     const res = await fetch('http://jsonplaceholder.typicode.com/users/');
//     const json = await res.json();
//     response.json(json);
//     console.log(json);
// }

// observables
app.get('/users', (req, res) => {
    from(fetch('http://jsonplaceholder.typicode.com/users/'))
        .pipe(
            map(data=>data.json())
        )
        .subscribe(data=>res.json(data));
});

app.listen(3000, ()=>console.log('server started on port 3000...'));