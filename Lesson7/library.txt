author:
{
    authorName: 'Bijesh',
    publishedBooks: [123456789, 987654321, 987612345]
}

book:
{
    book: 'Hello World',
    isbn: 123456789
    keywords: ['mongodb', 'express', 'angular', 'node']
}

student:
{
    _id: 123,
    studentName: 'Nitin',
    burrowedBooks: [
        {
            bookId: 123456789,
            burrowedDate: '06/04/2018',
            returnDate: '06/20/2018'
        }
    ]
}