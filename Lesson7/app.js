const express = require('express');
const mongoClient = require('mongodb').MongoClient;
const crypto = require('crypto');

const app = express();

app.get('/secret', (req, res)=>{
    mongoClient.connect('mongodb://127.0.0.1:27017/test', (err, client)=>{
        if(err) throw err;
        const db = client.db('test');
    
        db.collection('homework7').findOne({}, (err, doc)=>{
            if(err) throw err;
            res.send(decrypt(doc.message));
            client.close();
        });
    });
});

function decrypt(encrypted){
    const decipher = crypto.createDecipher('aes256', 'asaadsaad');
    let decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

app.listen(3000, ()=>console.log('Server started at port 3000...'));