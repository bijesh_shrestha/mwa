const fs = require('fs');
const server = require('http').createServer();

server.on('request', (req, res) => {
    // fs.readFile('./testImage.jpg', (err, data) => {
    //     if (err) throw err;
    //     res.end(data);
    // });

    const src = fs.createReadStream('./testImage.jpg');
    src.pipe(res);
});

server.listen(8000);
