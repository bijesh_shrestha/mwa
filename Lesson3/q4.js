var fs = require('fs');
var zlib = require('zlib');
var gzip = zlib.createGzip();
var gunzip = zlib.createGunzip();

var readable = fs.createReadStream(__dirname + '/testImage.jpg');
var compressed = fs.createWriteStream(__dirname + '/imageZip.jpg.gz');

readable.pipe(gzip).pipe(compressed);

// readable = fs.createReadStream(__dirname + '/imageZip.jpg.gz');
// compressed = fs.createWriteStream(__dirname + '/testImage.jpg');

// readable.pipe(gunzip).pipe(compressed);