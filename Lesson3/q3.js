const EventEmitter = require('events');

class Clock extends EventEmitter {
    constructor() {
        super();
    }
}

const clock = new Clock();
clock.on('tickListener', () => console.log('woohoo'));

setInterval(() => clock.emit('tickListener'), 1000);